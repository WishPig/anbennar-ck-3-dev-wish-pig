﻿# Island regions - no land path from the continent
# The AI needs these to optimize path finding
#
# NOTE: do not add any regions here that are NOT islands
#
# Island regions can be declared with one or more of the following fields:
#	duchies = { }, takes duchy title names declared in landed_titles.txt
#	counties = { }, takes county title names declared in landed_titles.txt
#	provinces = { }, takes province id numbers declared in /history/provinces

island_region_venail = {
	duchies = { d_venail }
}

island_region_gemisle = {
	counties = { c_gemisle }
}

island_region_pir_ail = {
	counties = { c_pir_ail }
}

island_region_bus_akanu = {
	counties = { c_bus_akanu }
}

island_region_bhetu = {
	counties = { c_bhetu }
}

island_region_moonmount = {
	provinces = { 9 }
}

# island_region_b_test_534 = {
# 	provinces = { 524 }
# }

island_region_nimscodd = {
	counties = { c_nimscodd }
}

island_region_storm_isles = {
	counties = { c_storm_isles }
}

island_region_lilcodd = {
	counties = { c_lilcodd }
}

island_region_knarhatt = {
	counties = { c_knarhatt }
}

island_region_jotunhamr = {
	duchies = { d_jotunhamr }
}