#k_sarwick
##d_sarwood
###c_sarwick
879 = {		#Sarwick

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2576 = {

    # Misc
    holding = church_holding

    # History

}
2577 = {

    # Misc
    holding = city_holding

    # History

}
2578 = {

    # Misc
    holding = none

    # History

}

###c_bogwood
872 = {		#Bogwood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2571 = {

    # Misc
    holding = city_holding

    # History

}
2572 = {

    # Misc
    holding = none

    # History

}

###c_stenced
878 = {		#Stenced

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2573 = {

    # Misc
    holding = church_holding

    # History

}
2574 = {

    # Misc
    holding = none

    # History

}
2575 = {

    # Misc
    holding = none

    # History

}

##d_kondunn
###c_kondunn
870 = {		#Kondunn

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2558 = {

    # Misc
    holding = church_holding

    # History

}
2559 = {

    # Misc
    holding = none

    # History

}

###c_alloysford
861 = {		#Alloysford

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2555 = {

    # Misc
    holding = none

    # History

}

###c_fogwood
863 = {		#Fogwood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2556 = {

    # Misc
    holding = city_holding

    # History

}
2557 = {

    # Misc
    holding = none

    # History

}

###c_canreced
868 = {		#Canreced

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2560 = {

    # Misc
    holding = city_holding

    # History

}
2561 = {

    # Misc
    holding = church_holding

    # History

}
2562 = {

    # Misc
    holding = none

    # History

}

##d_nortessord
###c_ordham
881 = {		#Ordham

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2567 = {

    # Misc
    holding = city_holding

    # History

}
2568 = {

    # Misc
    holding = none

    # History

}
2569 = {

    # Misc
    holding = none

    # History

}
2570 = {

    # Misc
    holding = none

    # History

}

###c_gulenhyl
871 = {		#Gulenhyl

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = gulenhyl_mines_01
		special_building = gulenhyl_mines_01
	}
}
2563 = {

    # Misc
    holding = church_holding

    # History

}
2564 = {

    # Misc
    holding = none

    # History

}

###c_watchers_wood
867 = {		#Watcher's Wood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2565 = {

    # Misc
    holding = church_holding

    # History

}
2566 = {

    # Misc
    holding = none

    # History

}

##d_esshyl
###c_esswyck
880 = {		#Esswyck

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2579 = {

    # Misc
    holding = church_holding

    # History

}
2580 = {

    # Misc
    holding = city_holding

    # History

}

###c_farhyl
884 = {		#Farhyl

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2583 = {

    # Misc
   holding = city_holding

    # History

}
2584 = {

    # Misc
    holding = none

    # History

}

###c_esswood
882 = {		#Esswood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2581 = {

    # Misc
    holding = church_holding

    # History

}
2582 = {

    # Misc
    holding = none

    # History

}
