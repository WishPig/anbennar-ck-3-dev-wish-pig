#k_blademarches
##d_blademarch
###c_bladeskeep
817 = {		#Bladeskeep

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2353 = {

    # Misc
    holding = city_holding

    # History

}
2354 = {

    # Misc
    holding = church_holding

    # History

}
2355 = {

    # Misc
    holding = none

    # History

}

###c_glitterdell
2350 = {		#Glitterdell

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
818 = {

    # Misc
    holding = city_holding

    # History

}
2349 = {

    # Misc
    holding = church_holding

    # History

}
2351 = {

    # Misc
    holding = none

    # History

}
2352 = {

    # Misc
    holding = none

    # History

}

###c_stewards_hold
819 = {		#Steward's Hold

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2345 = {

    # Misc
    holding = church_holding

    # History

}
2346 = {

    # Misc
    holding = city_holding

    # History

}
2347 = {

    # Misc
    holding = none

    # History

}
2348 = {

    # Misc
    holding = none

    # History

}

###c_dragonfell
816 = {		#Dragonfell

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2356 = {

    # Misc
    holding = church_holding

    # History

}
2357 = {

    # Misc
    holding = none

    # History

}

##d_medirleigh
###c_medirleigh
876 = {		#Medirleigh

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2362 = {

    # Misc
    holding = church_holding

    # History

}
2363 = {

    # Misc
    holding = city_holding

    # History

}
2364 = {

    # Misc
    holding = none

    # History

}
2365 = {

    # Misc
    holding = none

    # History

}

###c_boggartstead
877 = {		#Boggartstead

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2372 = {

    # Misc
    holding = church_holding

    # History

}
2373 = {

    # Misc
    holding = city_holding

    # History

}
2374 = {

    # Misc
    holding = none

    # History

}

###c_breakseben
875 = {		#Breakseben

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2369 = {

    # Misc
    holding = city_holding

    # History

}
2370 = {

    # Misc
    holding = none

    # History

}
2371 = {

    # Misc
    holding = none

    # History

}

###c_hendon
2366 = {	#Hendon

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
885 = {

    # Misc
    holding = city_holding

    # History

}
2367 = {

    # Misc
    holding = none

    # History

}
2368 = {

    # Misc
    holding = none

    # History

}

##d_swapstroke
###c_swapstroke
887 = {		#Swapstroke

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2360 = {

    # Misc
    holding = city_holding

    # History

}
2361 = {

    # Misc
    holding = none

    # History

}

###c_widnath
886 = {		#Widnath

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2358 = {

    # Misc
    holding = city_holding

    # History

}
2359 = {

    # Misc
    holding = none

    # History

}

##d_clovenwood
###c_clovenwood
811 = {		#Clovenwood

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2338 = {

    # Misc
    holding = church_holding

    # History

}
2339 = {

    # Misc
    holding = none

    # History

}

###c_satyrs_end
815 = {		#Satyr's End

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2340 = {

    # Misc
    holding = none

    # History

}

###c_feyvord
806 = {		#Feyvord

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2337 = {

    # Misc
    holding = none

    # History

}

##d_beastgrave
###c_banesfork
813 = {		#Banesfork

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2344 = {

    # Misc
    holding = city_holding

    # History

}

###c_beastgrave
812 = {		#Beastgrave

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2341 = {

    # Misc
    holding = none

    # History

}

###c_flaygarden
814 = {		#Flaygarden

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2342 = {

    # Misc
    holding = city_holding

    # History

}
2343 = {

    # Misc
    holding = none

    # History

}
