#k_deranne
##d_deranne
###c_deranne
113 = {		#Deranne

    # Misc
    culture = derannic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1142 = {

    # Misc
    holding = city_holding

    # History

}
1140 = {

    # Misc
    holding = castle_holding

    # History

}
1141 = {

    # Misc
    holding = none

    # History

}
1147 = {

    # Misc
    holding = none

    # History

}

###c_westport
112 = {		#Westport

    # Misc
    culture = derannic
    religion = court_of_adean
    holding = city_holding

    # History
}
1139 = {

    # Misc
    holding = castle_holding

    # History

}

###c_rosereach
111 = {		#Rosereach

    # Misc
    culture = derannic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1144 = {

    # Misc
    holding = none

    # History

}
1146 = {

    # Misc
    holding = none

    # History

}
1145 = {

    # Misc
    holding = city_holding

    # History

}

###c_clearshore
121 = {		#Clearshore

    # Misc
    culture = derannic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1143 = {

    # Misc
    holding = none

    # History

}

###c_aelvar
110 = {		#Aelvar

    # Misc
    culture = derannic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1137 = {

    # Misc
    holding = none

    # History

}
1138 = {

    # Misc
    holding = none

    # History

}

##d_darom
###c_darom
58 = {		#Daromsfort

    # Misc
    culture = entebenic
    religion = court_of_adean
    holding = castle_holding

    # History

}
1126 = {

    # Misc
    holding = city_holding

    # History

}
1127 = {

    # Misc
    holding = church_holding

    # History

}
1128 = {

    # Misc
    holding = none

    # History

}

###c_lencesk
102 = {		#Lencesk

    # Misc
    culture = derannic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1134 = {

    # Misc
    holding = none

    # History

}
1132 = {

    # Misc
    holding = church_holding

    # History

}


1133 = {

    # Misc
    holding = city_holding

    # History

}

1135 = {

    # Misc
    holding = city_holding

    # History

}
1136 = {

    # Misc
    holding = none

    # History

}

###c_hollowview
104 = {		#Hollowview

    # Misc
    culture = entebenic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1130 = {

    # Misc
    holding = city_holding

    # History

}
1131 = {

    # Misc
    holding = none

    # History

}

1129 = {

    # Misc
    holding = none

    # History

}
