﻿# Happens when a culture loses a tradition
# Root = culture
# scope:tradition = flag for the tradition removed
anb_on_tradition_removed = {
	effect = {
		if = {
			limit = {
				scope:tradition = {
					has_cultural_parameter = charaters_are_bilingual
				}
			}

			set_culture_languages_from_traditions = yes
		}
	}
}

# Happens when a culture gains a tradition
# Root = culture
# scope:tradition = flag for the tradition added
anb_on_tradition_added = {
	effect = {
		if = {
			limit = {
				scope:tradition = {
					has_cultural_parameter = charaters_are_bilingual
				}
			}

			set_culture_languages_from_traditions = yes
		}
	}
}
