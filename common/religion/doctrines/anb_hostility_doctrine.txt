﻿# Anbennar - added our own

hostility_group = {
	group = "not_creatable"
	abrahamic_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 2
			hostility_same_family = 3
			hostility_others = 3
		}
	}
	pagan_hostility_doctrine = {
		visible = no
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	eastern_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 1
			hostility_others = 2
		}
	}
	pantheonic_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 1	#astray, maybe turn it righteous
			hostility_same_family = 3 #currently only people in this are infernalists but tolerance is controlled elsewhere. May need to change this at some point.
			hostility_others = 2 #might need to change these but yeah
		}
	}
	faithless_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 1	#astray, maybe turn it righteous? maybe thats for regent court
			hostility_same_family = 1
			hostility_others = 3	#might need to change these but yeah
		}
	}
	sun_cult_hostility_doctrine = { # Placeholder
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	elven_hostility_doctrine = { # Placeholder
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	divenori_hostility_doctrine = { # Placeholder
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	skaldhyrric_hostility_doctrine = { # Placeholder
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	infernal_courts_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 2
			hostility_same_family = 3
			hostility_others = 3
		}
	}

	dragon_cult_hostility_doctrine = { #placeholder
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	dwarven_hostility_doctrine = { #placeholder
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 3
		}
	}
	precastanorian_hostility_doctrine = { 
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 2
		}
	}
}
