﻿anbenncost = {
	county = c_anbenncost
	barony = b_temple_district
	
	character_modifier = {
		name = holy_site_anbenncost_effect_name
		development_growth_factor = 0.1
		diplomacy = 1
	}
}

moonmount = {
	county = c_moonmount
	barony = b_temple_of_the_highest_moon
	
	character_modifier = {
		name = holy_site_moonmount_effect_name
		learning_per_piety_level = 1
	}
}

kheterat = {
	county = c_kheterat
	barony = b_kheterat
	
	character_modifier = {
		name = holy_site_kheterat_effect_name
		build_gold_cost = -0.2
		monthly_dynasty_prestige_mult = 0.1
	}
}

nirat = {
	county = c_nirat
	barony = b_nirat
	
	character_modifier = {
		name = holy_site_nirat_effect_name
		development_growth_factor = 0.15
	}
}

ibtat = {
	county = c_ibtat
	barony = b_ibtat
	
	character_modifier = {
		name = holy_site_ibtat_effect_name
		stress_loss_mult = 0.25
		councillor_opinion = 5
	}
}

golkora = {
	county = c_golkora
	barony = b_golkora
	
	character_modifier = {
		name = holy_site_golkora_effect_name
		stewardship_per_piety_level = 1
	}
}

the_necropolis = {
	county = c_corseton
	barony = b_the_necropolis

	character_modifier = {
		name = holy_site_the_necropolis_effect_name
		learning_per_piety_level = 1
	}
}

minar = {
	county = c_minar
	barony = b_minar
	
	character_modifier = {
		name = holy_site_minar_effect_name
		# Temp
	}
}

bulwar = {
	county = c_bulwar
	barony = b_bulwar
	
	character_modifier = {
		name = holy_site_bulwar_effect_name
		# Temp
	}
}

azkaszelazka = {
	county = c_azkaszelazka
	barony = b_azkaszelazka
	
	character_modifier = {
		name = holy_site_azkaszelazka_effect_name
		# Temp
	}
}

eduz_vacyn = {
	county = c_eduz_vacyn
	barony = b_eduz_vacyn
	
	character_modifier = {
		name = holy_site_eduz_vacyn_effect_name
		# Temp
	}
}

azka_sur = {
	county = c_azka_sur
	barony = b_azka_sur
	
	character_modifier = {
		name = holy_site_azka_sur_effect_name
		# Temp
	}
}

azka_szel_udam = {
	county = c_azka_szel_udam
	barony = b_azka_szel_udam
	
	character_modifier = {
		name = holy_site_azka_szel_udam_effect_name
		# Temp
	}
}

skaldol = {
	county = c_skaldol
	barony = b_skaldol
	
	character_modifier = {
		name = holy_site_skaldol_effect_name
		learning_per_piety_level = 1
	}
}

algrar = {
	county = c_algrar
	barony = b_algrar
	
	character_modifier = {
		name = holy_site_algrar_effect_name
		men_at_arms_maintenance = -0.05
		clergy_opinion = 5
	}
}

coldmarket = {
	county = c_coldmarket
	barony = b_coldmarket
	
	character_modifier = {
		name = holy_site_coldmarket_effect_name
		monthly_piety_gain_mult = 0.1
	}
}

jotunhamr = {
	county = c_jotunhamr
	barony = b_jotunhamr
	
	character_modifier = {
		name = holy_site_jotunhamr_effect_name
		naval_movement_speed_mult = 0.25
		embarkation_cost_mult = -0.25
	}
}

bal_vroren = {
	county = c_bal_vroren
	barony = b_bal_vroren
	
	character_modifier = {
		name = holy_site_bal_vroren_effect_name
		prowess_per_piety_level = 1
		supply_duration = 0.2
	}
}

# drekiriki = {
	# county = c_oddansbay
	# barony = b_oddansbay
	
	# character_modifier = {
		# name = holy_site_drekiriki_effect_name
		# knight_effectiveness_mult = 0.2
		# knight_limit = 1
	# }
# }

westport = {
	county = c_westport
	barony = b_westport
	
	character_modifier = {
		name = holy_site_westport_effect_name
		martial_per_piety_level = 1
		raid_speed = 0.1
	}
}

wisphollow = {
	county = c_wisphollow
	barony = b_wisphollow
	
	character_modifier = {
		name = holy_site_wisphollow_effect_name
	}
}

lioncost = {
	county = c_lioncost
	barony = b_lioncost
	
	character_modifier = {
		name = holy_site_lioncost_effect_name
	}
}

arca_corvur = {
	county = c_arca_corvur
	barony = b_arca_corvur
	
	character_modifier = {
		name = holy_site_arca_corvur_effect_name
	}
}

bal_dostan = {
	county = c_arca_corvur
	barony = b_arca_corvur
	
	character_modifier = {
		#Should be identical to arca_corvur
		name = holy_site_bal_dostan_effect_name
	}
}

silverforge = {
	county = c_silverforge
	barony = b_silverforge_hall
	
	character_modifier = {
		name = holy_site_silverforge_effect_name
	}
}

bal_vroren = {
	county = c_bal_vroren
	barony = b_bal_vroren
	
	character_modifier = {
		name = holy_site_bal_vroren_effect_name
	}
}

bal_hyl = {
	county = c_wexkeep
	barony = b_wexkeep
	
	character_modifier = {
		name = holy_site_bal_hyl_effect_name
	}
}

dragonforge = {
	county = c_south_castonath
	barony = b_dragonforge_hill
	
	character_modifier = {
		name = holy_site_dragonforge_effect_name
		stewardship_per_piety_level = 1
	}
}

venail = {
	county = c_venail
	barony = b_venail
	
	character_modifier = {
		name = holy_site_venail_effect_name
		naval_movement_speed_mult = 0.25
		embarkation_cost_mult = -0.25
	}
}

silent_repose = {
	county = c_havoral_peak
	barony = b_havoral_peak
	
	character_modifier = {
		name = holy_site_silent_repose_effect_name
		stress_loss_mult = 0.25
		councillor_opinion = 5
	}
}

celmaldor = {
	county = c_celmaldor
	barony = b_celmaldor
	
	character_modifier = {
		name = holy_site_celmaldor_effect_name
		stewardship_per_piety_level = 1
	}
}

moonhaven = {
	county = c_moonhaven
	barony = b_moonhaven
	
	character_modifier = {
		name = holy_site_moonhaven_effect_name
		county_opinion_add = 5
	}
}

sirik_peak = {
	county = c_sirik
	barony = b_sirik
	
	character_modifier = {
		name = holy_site_sirik_peak_effect_name
		learning = 2
	}
}

hapak_peak = {
	county = c_hapak
	barony = b_hapak
	
	character_modifier = {
		name = holy_site_hapak_peak_effect_name
		mountains_development_growth_factor = 0.2
		mountains_advantage = 5
	}
}

enteben = {
	county = c_enteben
	barony = b_enteben
	
	character_modifier = {
		name = holy_site_enteben_effect_name
		prowess_per_piety_level = 3
		martial = 1
	}
}

rewanfork = {
	county = c_rewanfork
	barony = b_rewanfork
	
	character_modifier = {
		name = holy_site_rewanfork_effect_name
		development_growth_factor = 0.15
	}
}

lencesk = {
	county = c_lencesk
	barony = b_merisseidd
	
	character_modifier = {
		name = holy_site_lencesk_effect_name
		travel_time = 0.15
		county_opinion_add = 10
	}
}

the_approach = {
	county = c_the_approach
	barony = b_aldegarde
	
	character_modifier = {
		name = holy_site_the_approach_effect_name
		light_cavalry_max_size_add = 1
		heavy_cavalry_max_size_add = 1
	}
}

north_citadel = {
	county = c_north_citadel
	barony = b_north_citadel
	
	character_modifier = {
		name = holy_site_the_approach_effect_name
	}
}

xhazobains_end = {
	county = c_xhazobains_end
	barony = b_bronhyl
	
	character_modifier = {
		name = holy_site_the_approach_effect_name
		martial_per_piety_level = 2
	}
}