﻿name_list_alenic = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {
		#EU4
		Adelar Alain Alen Arnold Camor Carlan Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
		Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
		Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Venac Vencan Walter Welyam Wystan
		Caylen Arman Adrien Adrian 

		#New
		Alenn Alann Lennic Lennard Jon Artur Eric Ben Bernard
		Garret Garrett Abram Alan Archibald Barnabas Bennett Brandon Branden Brenden Byron Byrne Thorburn Nathaniel

		Hugh Peter Ralph Robert Roger Simon Thomas Walter
		Alan Albert Alfred Andrew Anselm Arnold Baldrick David Edmund Edward Eric George Gerald Gilbert
		Gregory Humphrey Jordan Liel Mark Martin Matthew
		Randolph Reginald Stephen Teague Waleran

		#Vanilla - from Old Saxon
		Arnold
		Bertold Bertram Boddic Brun
		Dethard Detmar Donar Eggerd
		Ghert
		Hoger Humfried Jacob Jaspar
		Lembert
		Luder Marbold Mathias Michael
		Rudolf Steffen
		Wulff

		Osric
		
	}

	female_names = {
		#EU4
		Bella Clarya Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Athana

		#New
		Abigail Allison Annabel Belinda Bridget Deidre Heather Meade Melinda Rosamond Sybil 

		#old saxon
		Addila Agnes Anna Athela Bertha Bertrada Bia
		Brigida Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
		Hadwig Hasala Helene Hildegard Ida Imma
		Katharina Margarete Mathilde Oda

		Agatha Balthild Judith Mildrith  
		Margaret

		Beatrice Blanche Catherine Edith Ela Emma Isabel
		Isabella Juliana Margaret Mary Matilda Maud Sybilla

	}

	dynasty_of_location_prefix = "dynnp_of"
	
	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 30
	mat_grf_name_chance = 10
	father_name_chance = 25
		
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 20
	mat_grm_name_chance = 40
	mother_name_chance = 5
	
	

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_old_alenic = {

	cadet_dynasty_names = {
		"dynn_Hayles"
		{ "dynnp_of" "dynn_Norfolk" }
		"dynn_Rose"
		"dynn_Dodington"
		"dynn_Stawell"
		"dynn_Huntingdon"
		"dynn_Lockhart"
		"dynn_Wuffing"
		"dynn_Henry"
		"dynn_Hastings"
		"dynn_Puttoc"
		{ "dynnp_of" "dynn_Anglo-Saxony" }
		{ "dynnp_of" "dynn_Northampton" }
		"dynn_A_theling"
		"dynn_Moore"
		"dynn_Fysche"
		"dynn_Dall"
		"dynn_Slee"
		"dynn_Kiil"
		"dynn_Mel"
		"dynn_Holter"
		"dynn_Randolph"
		"dynn_Percy"
		"dynn_Despencer"
		"dynn_Bacon"
		"dynn_Reresby"
		"dynn_Cornwallis"
		"dynn_Clifford"
		"dynn_Carew"
		"dynn_Maddock"
		"dynn_Fortescue"
		{ "dynnp_de" "dynn_Ros" }
		"dynn_Howard"
		"dynn_Maudit"
		"dynn_Berkeley"
		"dynn_Barton"
		"dynn_Colvile"
		"dynn_Grey"
		"dynn_Darcy"
		{ "dynnp_de" "dynn_Garlande" }
		"dynn_Perry"
		"dynn_Bethell"
		"dynn_Perry"
		"dynn_Borthwick"
		{ "dynnp_de" "dynn_London" }
		{ "dynnp_of" "dynn_Hereford" }
		{ "dynnp_of" "dynn_Gloucester" }
		{ "dynnp_of" "dynn_Oxford" }
		{ "dynnp_of" "dynn_Salisbury" }
		{ "dynnp_of" "dynn_Surrey" }
		{ "dynnp_of" "dynn_Sussex" }
		{ "dynnp_of" "dynn_Hampshire" }
		{ "dynnp_of" "dynn_Dorset" }
		{ "dynnp_of" "dynn_Somerset" }
		{ "dynnp_of" "dynn_Devon" }
		{ "dynnp_of" "dynn_Exeter" }
		{ "dynnp_of" "dynn_Cumberland" }
		{ "dynnp_of" "dynn_York" }
		{ "dynnp_of" "dynn_Lancaster" }
		{ "dynnp_of" "dynn_Chester" }
		{ "dynnp_of" "dynn_Lincoln" }
		{ "dynnp_of" "dynn_Leicester" }
		{ "dynnp_of" "dynn_Derby" }
		{ "dynnp_of" "dynn_Shrewsbury" }
		{ "dynnp_of" "dynn_Northampton" }
		{ "dynnp_of" "dynn_Bedford" }
		{ "dynnp_of" "dynn_Norfolk" }
		{ "dynnp_of" "dynn_Suffolk" }
		"dynn_Crawford"
		"dynn_Tynedale"
		"dynn_Pudsey"
		"dynn_Marshal"
		"dynn_Rowe"
		{ "dynnp_de" "dynn_Criel" }
		"dynn_Ciesla"
		"dynn_Ross"
		"dynn_Wuffing"
		"dynn_Beorming"
		"dynn_Eorling"
		"dynn_A_bbing"
		"dynn_A_lling"
		"dynn_Wintaling"
	}

	dynasty_names = {
		"dynn_Hayles"
		{ "dynnp_of" "dynn_Norfolk" }
		"dynn_Rose"
		"dynn_Dodington"
		"dynn_Stawell"
		"dynn_Huntingdon"
		"dynn_Lockhart"
		"dynn_Wuffing"
		"dynn_Henry"
		"dynn_Hastings"
		"dynn_Puttoc"
		{ "dynnp_of" "dynn_Anglo-Saxony" }
		{ "dynnp_of" "dynn_Northampton" }
		"dynn_A_theling"
		"dynn_Moore"
		"dynn_Fysche"
		"dynn_Dall"
		"dynn_Slee"
		"dynn_Kiil"
		"dynn_Mel"
		"dynn_Holter"
		"dynn_Randolph"
		"dynn_Percy"
		"dynn_Despencer"
		"dynn_Bacon"
		"dynn_Reresby"
		"dynn_Cornwallis"
		"dynn_Clifford"
		"dynn_Carew"
		"dynn_Maddock"
		"dynn_Fortescue"
		{ "dynnp_de" "dynn_Ros" }
		"dynn_Howard"
		"dynn_Maudit"
		"dynn_Berkeley"
		"dynn_Barton"
		"dynn_Colvile"
		"dynn_Grey"
		"dynn_Darcy"
		{ "dynnp_de" "dynn_Garlande" }
		"dynn_Perry"
		"dynn_Bethell"
		"dynn_Perry"
		"dynn_Borthwick"
		{ "dynnp_de" "dynn_London" }
		{ "dynnp_of" "dynn_Hereford" }
		{ "dynnp_of" "dynn_Gloucester" }
		{ "dynnp_of" "dynn_Oxford" }
		{ "dynnp_of" "dynn_Salisbury" }
		{ "dynnp_of" "dynn_Surrey" }
		{ "dynnp_of" "dynn_Sussex" }
		{ "dynnp_of" "dynn_Hampshire" }
		{ "dynnp_of" "dynn_Dorset" }
		{ "dynnp_of" "dynn_Somerset" }
		{ "dynnp_of" "dynn_Devon" }
		{ "dynnp_of" "dynn_Exeter" }
		{ "dynnp_of" "dynn_Cumberland" }
		{ "dynnp_of" "dynn_York" }
		{ "dynnp_of" "dynn_Lancaster" }
		{ "dynnp_of" "dynn_Chester" }
		{ "dynnp_of" "dynn_Lincoln" }
		{ "dynnp_of" "dynn_Leicester" }
		{ "dynnp_of" "dynn_Derby" }
		{ "dynnp_of" "dynn_Shrewsbury" }
		{ "dynnp_of" "dynn_Northampton" }
		{ "dynnp_of" "dynn_Bedford" }
		{ "dynnp_of" "dynn_Norfolk" }
		{ "dynnp_of" "dynn_Suffolk" }
		"dynn_Crawford"
		"dynn_Tynedale"
		"dynn_Pudsey"
		"dynn_Marshal"
		"dynn_Rowe"
		{ "dynnp_de" "dynn_Criel" }
		"dynn_Ciesla"
		"dynn_Ross"
		"dynn_Wuffing"
		"dynn_Beorming"
		"dynn_Eorling"
		"dynn_A_bbing"
		"dynn_A_lling"
		"dynn_Wintaling"
	}
	
	male_names = {
		#EU4
		Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Ciramod Clarimond Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
		Elecast Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
		Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
		Caylen Arman Adrien Adrian

		#New
		Alenn Alann Lennic Lennard

		#Vanilla - from Old Saxon
		Abo Adalgar Adalgod Aelle Alebrand
		Altfrid Ansgar Arnd Arnold Asig Beneke Benno
		Bernhard Bertold Bertram Boddic Brun Burchard Cobbo Cord
		Dethard Detmar Donar Eggerd
		Erik Esiko Everd Ewald Freawine Gero Gerold Gevert
		Ghert Giselher Giselmar Goswin Hartwig
		Hengest Henneke Herberd Hinrik Hoger Hulderic Humfried Immed Jacob Jaspar Jochim
		Lembert Liemar Liudger
		Luder Ludwig Marbold Marquard Mathias Merten Michael
		Norbert Odo Ordulf Radke Reginbern Reinbern Reineke Reinmar Rudigar
		Rudolf Steffen Thankmar Theoderic Theodoric
		Theodwin Thimo Tobe Tymmeke Udo Unwan Viric Volkwin Volrad Walbert Waldemar Waldered Waltard
		Warin Wecta Wenzel Werneke Wichbert Wichmann Wicho Wigebert Wilbrand
		Wulff

		Beorn Eadric Eardwulf Eastmund Guthmund Leofric Morcar Ordgar Osmund Osric Wulf Wulfgar Wulfgeat Wulfhelm Wulfhere Cenwulf Ceolwulf Beornwulf
		Werestan Eadbald Eormenric
	}
	female_names = {
		#EU4
		Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela
		Coraline

		#old saxon
		Addila Adelheid Alof Agnes Anna Athela Beatrix Bertha Bertrada Bia Bisina Bithild
		Brigida Christina Diedke Eilika Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
		Glismod Hadwig Hasala Heilwig Helene Hildegard Hrothwina Ida Imma Irmgard Irminburg
		Jutta Katharina Kunigunde Luitgard Margarete Mathilde Mechthild Oda

		#For Gawedi
		Agatha Balthild Ecgfrida Judith Mildrith  
		Wulfhild Margaret Wulfwynn 	 
	}

	dynasty_of_location_prefix = "dynnp_of"


	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 30
	mat_grf_name_chance = 10
	father_name_chance = 25
		
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 20
	mat_grm_name_chance = 40
	mother_name_chance = 5

	

	mercenary_names = {
		{ name = "mercenary_company_hengist_retinue" coat_of_arms = "mc_hengist_retinue" }
		{ name = "mercenary_company_wakes_guard" coat_of_arms = "mc_wakes_guard" }
		{ name = "mercenary_company_horsas_band" coat_of_arms = "mc_horsas_band" }
	}
}

name_list_moorman = {
	dynasty_names = {
		dynn_Pattensen
		dynn_Schneider
		dynn_Theodericing
		dynn_Richfriding
		dynn_Abonid
		dynn_Smeldiging
		dynn_Unwanid
		dynn_Hessing
	}
	
	male_names = {
		#Vanilla - from Scottish
		Adam Alpin Alan Alastair Andrew
		Angus Archibald Arran Aulay Beathan Brian Brice Calum Carbrey Colban
		Colin Conall Conan David Dermid Donald Douglas Duff Dugald
		Duncan Edgar Edward Edwin Eric Evander Ewan Farquhar Fergus Fingal Findlay Frang
		Gavin Gilbert Gilbride Gillespie Gilpatrick Gilroy Giric Glenn Gregor
		Hamish Hector Hugh Iain Indulf James Kenneth Lachlan Laurence Lennon
		Lulach Malcolm Maldoven Maldred Malmure Marcas
		Matthew Michael Morgan Murdoch Murray Neil Ninian Oscar
		Patrick Radulf Ranald Richard Robert Roderick Ronald Rory Ross Roy
		Shaw Simon Somerled Stephan Sweeny Talore Thomas Torquil Walan
		Walter 


		#Vanilla - Added from others
		Jon Artur Nathaniel Byrne Byron
		
		#New
		Henric Heathcliff Hindley Hareton Linton Lockwood Bennett Campbell


	}
	female_names = {
		#Vanilla - from Scottish
		Ada Affraic Agnes Aileen Alice Anna Annabella Beitris Beathoc Bride
		Catriona Cecilia Christina Deirdre Derilla Donada Donella
		Edith Edna Effie Eilionoir Eimhir Eithne Ela Eleanor Elspeth Euna Eva Fenella Flora
		Forflissa Galiena Glenna Gormelia Helen Innes Iona Isabel Isla Isobel Julia
		Kenna Kirstin Lillias Lorna Malmure Maisie Malina Margaret Mariota Marjory Mary
		Martha Maud Maura Mirren Morag Morna Moyna Muriel
		Murron Nuala Rhonda Rodina Ronalda Ros Saundra Sheena Shona Slaine Sorcha Steacy Una

		#Vanilla - Added from others
		Catherine Isabella Ellen

		#New
		Cathy Frances
	}

	dynasty_of_location_prefix = "dynnp_of"
	bastard_dynasty_prefix = "dynnp_mur"
	
	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 30
	mat_grf_name_chance = 10
	father_name_chance = 25
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 20
	mat_grm_name_chance = 40
	mother_name_chance = 5
}

name_list_wexonard = {

	cadet_dynasty_names = {
		"dynn_Hayles"
		{ "dynnp_of" "dynn_Norfolk" }
		"dynn_Rose"
		"dynn_Dodington"
		"dynn_Stawell"
		"dynn_Huntingdon"
		"dynn_Lockhart"
		"dynn_Wuffing"
		"dynn_Henry"
		"dynn_Hastings"
		"dynn_Puttoc"
		{ "dynnp_of" "dynn_Anglo-Saxony" }
		{ "dynnp_of" "dynn_Northampton" }
		"dynn_A_theling"
		"dynn_Moore"
		"dynn_Fysche"
		"dynn_Dall"
		"dynn_Slee"
		"dynn_Kiil"
		"dynn_Mel"
		"dynn_Holter"
		"dynn_Randolph"
		"dynn_Percy"
		"dynn_Despencer"
		"dynn_Bacon"
		"dynn_Reresby"
		"dynn_Cornwallis"
		"dynn_Clifford"
		"dynn_Carew"
		"dynn_Maddock"
		"dynn_Fortescue"
		{ "dynnp_de" "dynn_Ros" }
		"dynn_Howard"
		"dynn_Maudit"
		"dynn_Berkeley"
		"dynn_Barton"
		"dynn_Colvile"
		"dynn_Grey"
		"dynn_Darcy"
		{ "dynnp_de" "dynn_Garlande" }
		"dynn_Perry"
		"dynn_Bethell"
		"dynn_Perry"
		"dynn_Borthwick"
		{ "dynnp_de" "dynn_London" }
		{ "dynnp_of" "dynn_Hereford" }
		{ "dynnp_of" "dynn_Gloucester" }
		{ "dynnp_of" "dynn_Oxford" }
		{ "dynnp_of" "dynn_Salisbury" }
		{ "dynnp_of" "dynn_Surrey" }
		{ "dynnp_of" "dynn_Sussex" }
		{ "dynnp_of" "dynn_Hampshire" }
		{ "dynnp_of" "dynn_Dorset" }
		{ "dynnp_of" "dynn_Somerset" }
		{ "dynnp_of" "dynn_Devon" }
		{ "dynnp_of" "dynn_Exeter" }
		{ "dynnp_of" "dynn_Cumberland" }
		{ "dynnp_of" "dynn_York" }
		{ "dynnp_of" "dynn_Lancaster" }
		{ "dynnp_of" "dynn_Chester" }
		{ "dynnp_of" "dynn_Lincoln" }
		{ "dynnp_of" "dynn_Leicester" }
		{ "dynnp_of" "dynn_Derby" }
		{ "dynnp_of" "dynn_Shrewsbury" }
		{ "dynnp_of" "dynn_Northampton" }
		{ "dynnp_of" "dynn_Bedford" }
		{ "dynnp_of" "dynn_Norfolk" }
		{ "dynnp_of" "dynn_Suffolk" }
		"dynn_Crawford"
		"dynn_Tynedale"
		"dynn_Pudsey"
		"dynn_Marshal"
		"dynn_Rowe"
		{ "dynnp_de" "dynn_Criel" }
		"dynn_Ciesla"
		"dynn_Ross"
		"dynn_Wuffing"
		"dynn_Beorming"
		"dynn_Eorling"
		"dynn_A_bbing"
		"dynn_A_lling"
		"dynn_Wintaling"
	}

	dynasty_names = {
		"dynn_Hayles"
		{ "dynnp_of" "dynn_Norfolk" }
		"dynn_Rose"
		"dynn_Dodington"
		"dynn_Stawell"
		"dynn_Huntingdon"
		"dynn_Lockhart"
		"dynn_Wuffing"
		"dynn_Henry"
		"dynn_Hastings"
		"dynn_Puttoc"
		{ "dynnp_of" "dynn_Anglo-Saxony" }
		{ "dynnp_of" "dynn_Northampton" }
		"dynn_A_theling"
		"dynn_Moore"
		"dynn_Fysche"
		"dynn_Dall"
		"dynn_Slee"
		"dynn_Kiil"
		"dynn_Mel"
		"dynn_Holter"
		"dynn_Randolph"
		"dynn_Percy"
		"dynn_Despencer"
		"dynn_Bacon"
		"dynn_Reresby"
		"dynn_Cornwallis"
		"dynn_Clifford"
		"dynn_Carew"
		"dynn_Maddock"
		"dynn_Fortescue"
		{ "dynnp_de" "dynn_Ros" }
		"dynn_Howard"
		"dynn_Maudit"
		"dynn_Berkeley"
		"dynn_Barton"
		"dynn_Colvile"
		"dynn_Grey"
		"dynn_Darcy"
		{ "dynnp_de" "dynn_Garlande" }
		"dynn_Perry"
		"dynn_Bethell"
		"dynn_Perry"
		"dynn_Borthwick"
		{ "dynnp_de" "dynn_London" }
		{ "dynnp_of" "dynn_Hereford" }
		{ "dynnp_of" "dynn_Gloucester" }
		{ "dynnp_of" "dynn_Oxford" }
		{ "dynnp_of" "dynn_Salisbury" }
		{ "dynnp_of" "dynn_Surrey" }
		{ "dynnp_of" "dynn_Sussex" }
		{ "dynnp_of" "dynn_Hampshire" }
		{ "dynnp_of" "dynn_Dorset" }
		{ "dynnp_of" "dynn_Somerset" }
		{ "dynnp_of" "dynn_Devon" }
		{ "dynnp_of" "dynn_Exeter" }
		{ "dynnp_of" "dynn_Cumberland" }
		{ "dynnp_of" "dynn_York" }
		{ "dynnp_of" "dynn_Lancaster" }
		{ "dynnp_of" "dynn_Chester" }
		{ "dynnp_of" "dynn_Lincoln" }
		{ "dynnp_of" "dynn_Leicester" }
		{ "dynnp_of" "dynn_Derby" }
		{ "dynnp_of" "dynn_Shrewsbury" }
		{ "dynnp_of" "dynn_Northampton" }
		{ "dynnp_of" "dynn_Bedford" }
		{ "dynnp_of" "dynn_Norfolk" }
		{ "dynnp_of" "dynn_Suffolk" }
		"dynn_Crawford"
		"dynn_Tynedale"
		"dynn_Pudsey"
		"dynn_Marshal"
		"dynn_Rowe"
		{ "dynnp_de" "dynn_Criel" }
		"dynn_Ciesla"
		"dynn_Ross"
		"dynn_Wuffing"
		"dynn_Beorming"
		"dynn_Eorling"
		"dynn_A_bbing"
		"dynn_A_lling"
		"dynn_Wintaling"
	}
	
	#Wexonard names are very much more Old Alenic type
	male_names = {

		#EU4
		Acromar Adelar Alain Alen Arnold Carlan Celgal Ciramod Clarimond Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
		Frederic Godrac Godric Godryc Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
		Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
		Caylen Arman Adrien Adrian

		#New
		Alenn Alann Lennic Lennard Lothane Lothar Artur Artus Oto Ottog Ottron Ottomac Ottolin

		#Vanilla - from Old Saxon
		Abo Adalgar Adalgod Aelle Alebrand
		Altfrid Ansgar Arnd Arnold Asig Beneke Benno
		Bernhard Bertold Bertram Boddic Brun Cobbo Cord
		Dethard Detmar Donar Eggerd
		Erik Esiko Everd Ewald Freawine Gero Gerold Gevert
		Ghert Giselher Giselmar Goswin Hartwig
		Hengest Henneke Herberd Hinrik Hoger Hulderic Humfried Immed Jacob Jaspar Jochim
		Lembert Liemar Liudger
		Luder Ludwig Marbold Marquard Mathias Merten Michael
		Norbert Odo Ordulf Radke Reginbern Reinbern Reineke Reinmar Rudigar
		Rudolf Steffen Thankmar
		Theodwin Thimo Tobe Tymmeke Unwan Viric Volkwin Volrad Walbert Waldemar Waldered Waltard
		Warin Wenzel Werneke Wicho Wigebert Wilbrand
		Wulff

		Beorn Eadric Eardwulf Eastmund Guthmund Leofric Morcar Ordgar Osmund Osric Wulf Wulfgar Wulfgeat Wulfhelm Wulfhere Cenwulf Ceolwulf Beornwulf
		Werestan Eadbald Eormenric
	}

	female_names = {
		#EU4
		Clarimonde Clarya Clothilde Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Edith Elenor Emma
		Cora Corina Cecille Valence Athana

		#old saxon
		Addila Adelheid Alof Agnes Anna Athela Beatrix Bertha Bertrada Bia Bisina Bithild
		Brigida Christina Diedke Eilika Elisabeth Enda Frederuna Geilana Gerberga Gertrud Gisela
		Glismod Hadwig Hasala Heilwig Helene Hildegard Ida Imma Irmgard Irminburg
		Jutta Katharina Kunigunde Luitgard Margarete Mathilde Mechthild

		#For Gawedi
		Agatha Balthild Ecgfrida Judith Mildrith  
		Wulfhild Margaret Wulfwynn 	 

	}

	dynasty_of_location_prefix = "dynnp_of"


	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 30
	mat_grf_name_chance = 10
	father_name_chance = 25
		
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 20
	mat_grm_name_chance = 40
	mother_name_chance = 5

	

	mercenary_names = {
		{ name = "mercenary_company_hengist_retinue" coat_of_arms = "mc_hengist_retinue" }
		{ name = "mercenary_company_wakes_guard" coat_of_arms = "mc_wakes_guard" }
		{ name = "mercenary_company_horsas_band" coat_of_arms = "mc_horsas_band" }
	}
}