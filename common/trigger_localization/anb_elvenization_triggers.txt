﻿# culture_is_elvenized_trigger = {
	# global = ELVENIZED_CULTURE
	# global_not = NOT_ELVENIZED_CULTURE
	# third = ELVENIZED_CULTURE
	# third_not = NOT_ELVENIZED_CULTURE
# }

can_adopt_elvenization_innovation = {
	global = ELVENIZATION_REQUIREMENTS
	global_not = ELVENIZATION_REQUIREMENTS
	third = ELVENIZATION_REQUIREMENTS
	third_not = ELVENIZATION_REQUIREMENTS
}