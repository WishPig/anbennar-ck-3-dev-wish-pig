﻿#Anbennar - magical affinity

#Anbennar
has_magical_affinity_trigger = {
	has_trait = magical_affinity
}

has_skaldic_tale_active = {
	OR = {
		has_character_modifier = skaldhyrric_dirge
		has_character_modifier = skaldhyrric_gjalund
		has_character_modifier = skaldhyrric_beralic
		has_character_modifier = skaldhyrric_voyage
		has_character_modifier = skaldhyrric_golden_forest
		has_character_modifier = skaldhyrric_master_boatbuilders
		has_character_modifier = skaldhyrric_dragon_and_skald
		has_character_modifier = skaldhyrric_old_winter_lullaby
	}
}

can_move_title_into_gerudia_trigger = {
	trigger_if = {
		limit = {
			title_is_valid_to_move_to_gerudia = { TITLE = $TITLE$ }
		}
		completely_controls = $TITLE$
	}
	trigger_else = {
		# TODO - Add a tooltip that says "has not moved title to Gerudia"
		always = no
	}
}

title_is_valid_to_move_to_gerudia = {
	title_has_de_jure = { TITLE = $TITLE$ }
	$TITLE$ = {
		NOT = { empire = title:e_gerudia } 
		NOT = { has_variable = kingdom_moved_into_gerudia_empire }
	}
}

title_has_de_jure = {
	$TITLE$ = { any_in_de_jure_hierarchy = { tier = tier_county } }
}
